#!/bin/sh

########## CONSTANTS ##########################################################

# Usage information
USAGE="Usage: $0 <pdb-id>"
# Output file directory, no trailing slash
DEST=./proteins/pdb
# Source directory (HTTP)
SRC="http://www.rcsb.org/pdb/files/"
# Uncomment for debug info
DEBUG=$2
# Temp file for wget status handling
WGET_OUT=.wget.out
PDB_ID=$1

########## FUNCTIONS ##########################################################

exists() {
  type "$1" >/dev/null 2>&1 || return 1
  return 0
}
fail() { # catch error status codes
  case $1 in
  000)
    echo "No internet connection." 1>&2 
    ;;
  301|302|404) # 302 is redirect to a 404 page
    echo "Invalid PDB ID (No such entry: $PDB_ID)" 1>&2 
    ;;
  *)
    echo "Error code: $1" 1>&2    
  esac
  echo "Fetch failed" 1>&2
  # Need to remove file if using wget
  #[ $2 ] && [ $2 = wget ] && 
  rm -f $DEST/$PDB_ID.pdb.gz $DEST/$PDB_ID.pdb
  exit $1
}
parsewgetout() {
   response=$(fgrep "HTTP request sent," $WGET_OUT \
       | head -1 | sed 's/[^0-9]//g')
   rm $WGET_OUT
   [ $response ] || fail 000 wget
   [ $response -eq 200 ] || fail $response wget
}

########## SCRIPT #############################################################

if [ ! $1 ]
then
  echo $USAGE
  exit 1
fi
  
mkdir -p $DEST
  
# Check if gunzip and curl are avail., fall back to wget and uncompressed files
if exists curl && exists gunzip
then
  [ $DEBUG ] && echo "FETCHING using curl and gunzip"
  response=$(curl $SRC$1.pdb.gz -s -w %{http_code} \
      -o $DEST/$1.pdb.gz)
  [ $response -eq 200 ] || fail $response
  gunzip -f $DEST/$1.pdb.gz
elif exists curl
then
  [ $DEBUG ] && echo "FETCHING using wget"
  response=$(curl $SRC$1.pdb -s -w %{http_code} \
      -o $DEST/$1.pdb)
  [ $response -eq 200 ] || fail $response
elif exists wget && exists gunzip
then
  [ $DEBUG ] && echo "FETCHING using wget and gunzip"
  wget $SRC$1.pdb.gz -P $DEST -o $WGET_OUT
  parsewgetout
  gunzip -f $DEST/$1.pdb.gz
elif exists wget
then
  [ $DEBUG ] && echo "FETCHING using wget"
  wget $SRC$1.pdb -P $DEST  -o $WGET_OUT
  parsewgetout
else
  echo "Require wget or curl to download PDB files." 1>&2;
  echo "Fetch failed" 1>&2
  exit 2
fi

echo "Fetch succeeded"
exit 0
