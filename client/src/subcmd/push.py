import socket
from lib import io
from subcmd import SubCommand
from lib.fileinfo import PDBFile

class PushSC(SubCommand):
  "Push"
  @staticmethod
  def execute(args): # args.pdb
    pfile = PDBFile(args.pdb_id)
    file_path = pfile.obj
    server = args.remote_ip
    pid  = args.pdb_id.upper()
    port = 4242
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(10) # 10 second timeout
    # host = sock.gethostname()
    print "Connecting to server at", server
    try:
      sock.connect((server, port))
    except Exception, e:
      print "Unable to connect to server, aborting."
      return
      
    sock.settimeout(None) # blocking mode
    # Send header
    sock.send(pid)
    title = pfile.raw_title[:80]
    padd = 80 - len(title)
    title += ("\0" * padd)    
    sock.send(title)
    # Wait for response
    status = sock.recv(1)
    if status == 'L':
      # socket is listening, send file
      print "Transferring", file_path
      with open(file_path, 'r') as f:
        chunk = f.read(1024)
        while chunk:
          sock.sendall(chunk)
          chunk = f.read(1024)
          
    print "Transfer complete"
    sock.close()
