import os, subprocess, signal, select, time, copy, sys
from subcmd import SubCommand
from fetch  import FetchSC
from lib import io

class CompileSC(SubCommand):
  "Compile"
  verbose = False
  chimera = "chimera" # "../chimera/bin/chimera"
  @staticmethod
  def execute(args):
    if args.verbose: CompileSC.verbose = True
    if len(args.pdb_id) != 4 or not args.pdb_id.isalnum():
      print "Invalid PDB ID"
      return False
    args.pdb_id = args.pdb_id.upper()
    # Check if exists
    if not (os.path.isfile("./proteins/pdb/%s.pdb" % args.pdb_id)):
      # Fetch if does not
      if not io.query_yes_no("File [%s] does not exist locally, download?" % args.pdb_id):
        return False        
      else:
        fargs = copy.copy(args)
        fargs.pdb_id = [args.pdb_id]
        if not FetchSC.execute(fargs):
          return False

    CompileSC.pdb_to_wrl(args.pdb_id)
    CompileSC.wrl_to_obj(args.pdb_id)
    #CompileSC.obj_fix() s:/[0-9]+::g
    print "Compilation successful"

  @staticmethod
  def pdb_to_wrl(pdb_id):          
    ### Use Chimera to convert from PDB to WRL
    script = "./src/lib/chimera_pdb_wrl.py"    # script to invoke conversion/exportation
    source = "./proteins/pdb/%s.pdb" % pdb_id  # source PDB file
    dest   = "./proteins/wrl/%s.wrl" % pdb_id  # output VRML file
    lines = [
      "open %s" % os.path.abspath(source),
      "export format VRML %s" % os.path.abspath(dest),
      "close all",
      "stop now"]
    import random
    fnum = random.randint(0, sys.maxint)
    fname = '.%s_%d.cmd' % (pdb_id, fnum)
    with open(fname, 'w') as tf:
      tf.seek(0)
      for line in lines: 
        tf.write(line + '\n')
      tf.flush()
      if CompileSC.verbose:
        print "Rendering from PDB to WRL using `Chimera'..."
      subprocess.call([CompileSC.chimera, tf.name])
    os.unlink(fname)
      
  @staticmethod
  def wrl_to_obj(pdb_id):    
    ### Use meshconv to convert from WRL to OBJ
    meshconv = ["./bin/meshconv", 
                "wrl",                                  # source format
                "-c", "obj",                            # output format 
                # "-rt",                                  # relay texture coordinates
                "-tri",                                 # triangulate mesh
                "./proteins/wrl/%s.wrl" % pdb_id,       # source file
                "-kz",                                  # keep zero area polygons
                "-o", "./proteins/obj/%s" % pdb_id]     # destination file

    if CompileSC.verbose:
      sys.stdout.write("Converting from WRL to OBJ using `meshconv'")
      sys.stdout.flush()
      with open(os.devnull, 'w') as devnull:
        p = subprocess.Popen(meshconv, stdout=devnull)
        b = True
        while p.poll() == None:
          time.sleep(0.25)
          b = not b
          if b:
            sys.stdout.write('.')
            sys.stdout.flush()
        print
    else:          
      with open(os.devnull, 'w') as devnull:
        subprocess.call(meshconv, stdout=devnull)
      
