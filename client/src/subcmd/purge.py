import os
from lib import io
from subcmd import SubCommand

class PurgeSC(SubCommand):
  "Purge"
  @staticmethod
  def execute(args): # args.pdb, args.obj
	
    if (not args.obj and not args.pdb):
      # remove all
      if (io.query_yes_no("Are you sure you want to remove all files?", default="no")):
        args.func.remove_compiled()
        args.func.remove_uncompiled()

      if args.obj:
        # show only compiled
        args.func.remove_compiled()
      if args.pdb:
        # show only uncompiled
        args.func.remove_uncompiled()
      
  @staticmethod
  def remove_uncompiled():
    folder = 'proteins/pdb/'
    io.remove_all_files_in_dir(folder)
    print "Removed all local PDB files"
  
  @staticmethod
  def remove_compiled():
    folder = 'proteins/obj/'
    io.remove_all_files_in_dir(folder)
    print "Removed all local OBJ files"

  
