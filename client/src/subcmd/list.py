import os
from subcmd import SubCommand
from lib.fileinfo import PDBFile
from operator import attrgetter

class ListSC(SubCommand):
  "List"
  @staticmethod
  def execute(args): # args.pdb, args.obj, args.id_only
    # TODO validate pdb_id
    if (not args.obj and not args.pdb):
      # show all
      args.func.print_compiled(id_only=args.id_only)
      args.func.print_uncompiled(id_only=args.id_only)

    if args.obj:
      # show only compiled
      args.func.print_compiled(False, args.id_only)
    if args.pdb:
      # show only uncompiled
      args.func.print_uncompiled(False, args.id_only)
      
  @staticmethod
  def print_uncompiled(header=True, id_only=True):
    if (header):
      print "+-------------------+"
      print "|UNCOMPILED PROTEINS|"
      print "+-------------------+"
    ListSC.print_dir("./proteins/pdb/", id_only)
      
  @staticmethod
  def print_compiled(header=True, id_only=True):
    if (header):
      print "+-------------------+"
      print "| COMPILED PROTEINS |"
      print "+-------------------+"
    ListSC.print_dir("./proteins/obj/", id_only)
  
  @staticmethod
  def print_dir(folder, id_only=True):
    fs = os.listdir(folder)
    if not len(fs): 
      print None
      return
    files = sorted([PDBFile(f[:4]) for f in fs], key=attrgetter('title'))
    if id_only:
      print "\n".join(["%s"   % f.id   for f in files])   
    else:
      print "\n".join(["%s\n" % str(f) for f in files])
    
 
