import os
from subcmd import SubCommand
from lib import io, fileinfo
#
# Parameters:
#    args.pdb_id
#
class FetchSC(SubCommand):
  "Fetch"
  verbose = False

  @staticmethod
  def execute(args): 
    boolall = True
    for pdb in args.pdb_id:
      if args.verbose:
        FetchSC.verbose = True
      if not FetchSC.get_pdb(pdb.upper()):
        boolall = False
        
    return boolall # all files succeeded
    
  @staticmethod
  def get_pdb(pdb_id):
    # Validate ID
    if (len(pdb_id) != 4 or not pdb_id.isalnum()):
       print "Invalid PDB ID (Must be 4 alphanumeric characters)"
       return False
    # Check if exists
    if (os.path.isfile("proteins/pdb/%s.pdb" % pdb_id) 
    and not io.query_yes_no(
          "File [%s] already exists locally, download again?" % pdb_id)):
        return False        
    # Build command  
    command = ["src/shell/pdb-fetch.sh", pdb_id]
    if (FetchSC.verbose):
      command.append("DEBUG") 
      print "Fetching protein", pdb_id
    # Execute command
    os.system(" ".join(command));
    
    # Print file name
    try:
      f = fileinfo.PDBFile(pdb_id) 
      print f, '\n'
    except:
      return False
     
    return True

