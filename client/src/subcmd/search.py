import os, re, urllib, urllib2, sys
from pydoc        import pager as print_paged
from subcmd       import SubCommand
from sets         import Set
from lib          import io, sanitize
from lib.fileinfo import PDBFile, PDBObject
from lxml         import etree  
from operator     import attrgetter
  
class SearchSC(SubCommand):
  "Search"
  verbose = False
  
  @staticmethod
  def execute(args): # args.keyword[], args.local
    SearchSC.verbose = args.verbose
    if (not args.local):
       return SearchSC.web_search(args.keyword)
    pdbs = sorted(os.listdir('proteins/pdb/'))
    results = Set()
    for pdb in pdbs:
      pfile = PDBFile(pdb.split(".")[0])
      for k in args.keyword:
        k = k.upper()
        if (k in pfile.raw_header
        or  k in pfile.raw_title
        or  k in pfile.id):
          results.add(pfile)
    for r in results:
       print r, '\n'

    return True
 
  @staticmethod
  def web_search(kwords):
    # TODO Add cli options for specific searches. Default to ALL
    query_dict = {
      'TokenKeywordQuery' : """
        <orgPdbQuery>
          <queryType>%s</queryType>
          <struct_keywords.pdbx_keywords.comparator>contains</struct_keywords.pdbx_keywords.comparator>
          <struct_keywords.pdbx_keywords.value>%s</struct_keywords.pdbx_keywords.value>
        </orgPdbQuery>
        """,
      'ChemCompNameQuery' : """
        <orgPdbQuery>
          <queryType>%s</queryType>
          <comparator>Contains</comparator>
          <name>%s</name>
          <polymericType>Any</polymericType>
        </orgPdbQuery>
        """,
      'StructTitleQuery' : """
        <orgPdbQuery>
          <queryType>%s</queryType>
          <struct.title.comparator>contains</struct.title.comparator>
          <struct.title.value>%s</struct.title.value>
        </orgPdbQuery>
        """,
     'StructDescQuery' : """     
        <orgPdbQuery>
          <queryType>%s</queryType>
          <entity.pdbx_description.comparator>contains</entity.pdbx_description.comparator>
          <entity.pdbx_description.value>%s</entity.pdbx_description.value>
        </orgPdbQuery>
        """,
    }    
    
    num = 0
    path = ('org','pdb','query','simple')
    count = len(query_dict)
    string = None
    wl_dict = {}
    kw_string = sanitize.xml_remove_unsafe(" ".join(kwords))
    results_dict = {}
    for query_type, query in query_dict.iteritems():
      num += 1
      string = "[Request {} of {}] ".format(num, count)
      io.clear_terminal_line(len(string))
      io.write(string)
      formatter = ('.'.join(map(str, path) + [query_type]), kw_string)
      results_dict[query_type] = []
      if SearchSC.verbose:
        print "Querying: %s..." % query_type
        print query % formatter
                
      # Make requests  
      for pdb in SearchSC.post_search_query(query % formatter):
        if len(pdb) == 4:
          results_dict[query_type].append(pdb)
        elif len(pdb) == 8:
          wl_dict[query_type] = pdb 
    # clear status line
    io.clear_terminal_line()

    # Join results
    results = Set()
    for group in results_dict.values():
      [results.add(r) for r in group]
    # print results
    if len(results):
      if len(results) > 30:
        # print directly to filter without info
        print_paged('\n'.join(results))        
      else: 
        # pull up info
        objects = SearchSC.post_describe_query(results)
        objects.sort(key=attrgetter('title'))
        (width, height) = io.get_terminal_size()
        if len(objects) * 4 > height and io.query_yes_no(
            "Too many results for one console window. Print to pager instead?"):
          print_paged('\n-------\n'.join(map(PDBObject.cli_str, objects)))
        else:
          print '\n-------\n'.join(map(PDBObject.cli_str, objects)) + '\n'
        
    # Web results
    web_results_url = "http://pdb.org/pdb/results/results.do?qrid="
    for s_type, ref in wl_dict.iteritems():
      length = len(results_dict[s_type])
      if length > 0:
        print "Result for '{0}' search [{3} results]:\n{4}{1}{2}\n".format(
              s_type, web_results_url, ref, length, ' '*8) 
  
    print "%d unique results total." % len(results)
    
  @staticmethod
  def post_search_query(query):
    url = "http://rcsb.org/pdb/rest/search?req=browser"
    req = urllib2.Request(url=url, data=query, 
          headers={'Content-Type': 'application/x-www-form-urlencoded'})
    response = urllib2.urlopen(req)
    page = response.read()
    
    return page.split('\n')
  
  @staticmethod
  def post_describe_query(results):
    
    url = 'http://rcsb.org/pdb/rest/describePDB?structureId='
    url += ','.join(results)     
    if SearchSC.verbose:
      print "Querying descriptions:"
      print ' '*8 + '%s\n' % url
 
    req = urllib2.Request(url=url, 
          headers={'Content-Type': 'application/x-www-form-urlencoded'})
    response = urllib2.urlopen(req)
    page = response.read()
    try:
      xml_root = etree.XML(page)
    except Exception as e:
      print e
    objects = []
    for c in xml_root:
      objects.append(PDBObject(
            c.get('structureId').upper(),
            c.get('keywords').upper(),
            c.get('title').upper()
      ))
    
    return objects
    
    
  # test dynamic XML building:
  #def build_query(query_type):
    #pdb_query  = etree.Element('orgPdbQuery')
    #query_type = etree.Element('queryType')
    #query_type.text = 'org.pdb.query.simple.TokenKeywordQuery'
    #comparator = etree.Element('struct_keywords.pdbx_keywords.comparator')
    #comparator.text = 'contains'
    #value      = etree.Element('struct_keywords.pdbx_keywords.value')
    #value.text      = '%s'
    #pdb_query.append(query_type)
    #pdb_query.append(comparator)
    #pdb_query.append(value)
    #return etree.tostring(root, pretty_print=True)

    
