from xml.sax.saxutils import escape, unescape

def xml_remove_unsafe(string):
  unsafe = "\"'<>&"
  return filter(lambda c: c not in unsafe, string)
  
def xml_escape(string):
  return escape(string)

def xml_unescape(string):
  return unescape(string)
