import sys, os, re, io

class PDBFile(object):
  def __init__(self, pdbid):
    #print os.getcwd()
    pdbid = pdbid.upper()
    self.path = "proteins/pdb/%s.pdb" % pdbid
    self.obj = "proteins/obj/%s.obj" % pdbid
    if not os.path.isfile(self.path):
      raise Exception("File not downloaded")
    self.id = pdbid
    self.raw_title, self.title   = self.parse_title()
    self.raw_header, self.header = self.parse_header()
  
  def __repr__(self):
    return repr((self.raw_title, self.raw_header, self.id))
 
  def __str__(self):
    return "\n".join(["PDB ID:  %s" % self.id,
                      "Header:  %s" % self.header,
                      "Title:   %s" % self.raw_title])
   
  def parse_title(self):
    ofile = ".parse_title.out"
    command = ["head", self.path, "|", "grep", "TITLE", ">", ofile]
    os.system(" ".join(command))
    lines = io.get_lines_from_file(ofile)
    os.system("rm %s" % ofile)
    if not lines: return "NO TITLE"
    title = ""
    for line in lines:
      title += re.compile("TITLE[\s]+[0-9]{0,2}[\s]{0,1}").split(line)[1] 

    title = re.sub(' +', ' ', title).strip()
    return title, title.title()
    
  def parse_header(self):
    ofile = ".parse_header.out"
    command = ["head", self.path, "|", "grep", "HEADER", ">", ofile]
    os.system(" ".join(command))
    lines = io.get_lines_from_file(ofile)
    os.system("rm %s" % ofile)
    if (not lines):
      return "NO HEADER"
    header = re.compile("HEADER[\s]+").split(lines[0])[1]        # remove HEADER tag
    header = re.compile("[0-9]{1,2}-[A-Z]{3}").split(header)[0]  # remove date 
    header = re.sub(' +', ' ', header).strip()                   # trim excess white space
    return header, header.title()
    
class PDBObject(object):
  "PDB Object for holding PDB info for files that aren't downloaded"
  def __init__(self, pdbid, header, title):
    self.id     = pdbid
    self.title  = title
    self.header = header
  
  def __repr__(self):
    return repr((self.title, self.header, self.id))
 
  def __str__(self):
    return "\n".join(["PDB ID:  %s" % self.id,
                      "Header:  %s" % self.header,
                      "Title:   %s" % self.title]) 
  def cli_str(self):
    (width, height) = io.get_terminal_size()
    return "\n".join(
        ["PDB ID:  %s" % self.id,
         "Header:  %s" % self.header,
         "Title:   %s" % (self.title if   len(self.title) < width 
                                     else self.title[:(width-12)] + '...')]) 

