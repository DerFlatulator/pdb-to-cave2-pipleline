__import__('sys').dont_write_bytecode = True

import argparse
from subcmd import fetch, list, purge, push, compile_pdb #, search #REQUIRES lxml MODULE

def main():
  # create the top-level parser
  parser = argparse.ArgumentParser(
      description='Pipeline for proteins from the PDB into CAVE2')
  parser.add_argument('-v', '--verbose', action='store_true',
      help='print more information at run time')
  sub_p = parser.add_subparsers(title='subcommands', description=
      """use one of the following subcommands;
         for subcommand specific help enter the subcommand followed by ` -h'""")
  
  pdb_id_help = 'the PDB ID code, e.g. 4Q21'
  
  # create the parser for the "fetch" command
  p_fetch = sub_p.add_parser('fetch', help='fetch a PDB file from the web')
  p_fetch.add_argument('pdb_id', nargs='+', help=pdb_id_help)
  p_fetch.set_defaults(func=fetch.FetchSC)
 
  # create the parser for the "compile" command
  p_compile = sub_p.add_parser('compile', help='compile PDB files')
  p_compile.add_argument('pdb_id', help=pdb_id_help)
  p_compile.set_defaults(func=compile_pdb.CompileSC)
 
  # create the parser for the "push" command
  p_push = sub_p.add_parser('push', help='push a protein to the CAVE2')
  p_push.add_argument('pdb_id', help=pdb_id_help)
  p_push.add_argument('remote_ip', help='The remote (server) IP address')
  p_push.set_defaults(func=push.PushSC)

  # create the parser for the "purge" command
  p_purge = sub_p.add_parser('purge', help='clean local protein repository')
  p_purge.add_argument('-o', '--obj', action='store_true',
      help='remove ALL compiled (.obj) proteins')
  p_purge.add_argument('-p', '--pdb', action='store_true',
      help='remove ALL uncompiled (.pdb) proteins')  
  p_purge.set_defaults(func=purge.PurgeSC)

  # create the parser for the "search" command
  #p_search = sub_p.add_parser('search', help='search data bank for proteins')
  #p_search.add_argument('-l', '--local', action='store_true',
  #    help='search locally')  
  #p_search.add_argument('keyword', nargs='+', 
  #    help='list of keywords to search for')
  #p_search.set_defaults(func=search.SearchSC)

  # create the parser for the "list" command
  p_list = sub_p.add_parser('list', help='list local proteins')
  p_list.add_argument('-o', '--obj', action='store_true',
      help='only compiled (.obj) proteins')
  p_list.add_argument('-p', '--pdb', action='store_true',
      help='display uncompiled (.pdb) proteins')  
  p_list.add_argument('-i', '--id-only', action='store_true',
      help='only print IDs')  
  p_list.set_defaults(func=list.ListSC)

  # parse arguments
  args = parser.parse_args()

  # execute command
#  try:
  args.func.execute(args)
#  except AttributeError as e:
#    print "Sub-command not implemented"
#    print e
#  except Exception as e:
#    print e
        
if __name__ == "__main__":
    main()
