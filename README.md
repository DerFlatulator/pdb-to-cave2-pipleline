Protein Data Bank to CAVE2 Pipleline
====================================

Developed by: Lucas Azzola

*NB:* Currently must be executed from the client or server.
The shell script `client/pdb2cave` directly passes args to
 `client/src/pdb2cave.py`, and `pdb2cave-server` launches
 either the model viewer or the listener depending on whether
 the `listen` option is applied.

See final report for usage instructions.
