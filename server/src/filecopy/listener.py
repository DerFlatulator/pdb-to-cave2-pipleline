import socket, sys

class Listener:
  "Server, listens for clients over socket, and transfers files."
  def __init__(self, port=4242):
    self.port = port
    self.listening = False
   
  def listen(self):
    #create an INET, STREAMing socket
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.host = socket.gethostname()
    self.socket.bind((self.host, self.port))
    self.socket.listen(1) # only accept one connection at a time
    self.listening = True
    print "[PDB_SERVER] Host: %s, Port: %d" % (self.host, self.port)
    print "[PDB_SERVER] Listening..."
    while self.listening:
      # accept connection
      client, address = self.socket.accept()
      pdbid = client.recv(4)
      title = client.recv(80)
      try:
        ind = title.index("\0")
      except:
        ind = 80
      title = title[:ind]
      if not pdbid.isalnum(): continue # reject 
      
      import os; print os.getcwd()
      print "[PDB_SERVER] Received new connection from host: %s" % str(address)
      print "[PDB_SERVER] receiving file: %s" % pdbid 
      client.send('L')
      # currently will overwrite any file
      with open("./proteins/%s.name" % pdbid, 'w') as f:
        f.write(title)
      with open("./proteins/%s.obj" % pdbid, 'w') as f:
        data = True
        while data:
          data = client.recv(1024)
          f.write(data)
              
      client.close()
      
      yield pdbid, title
      
    # end while
    self.socket.close()
      
  def abort(self):
    print "\n[PDB_SERVER] Closing connections"
    self.listening = False
    
  def abort_force(self):
    print "\n[PDB_SERVER] Abruptly closing connections"
    self.listening = False    
    self.socket.close()
    sys.exit(1)

