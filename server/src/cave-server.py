import sys, signal, os, time, threading
sys.dont_write_bytecode = True

from filecopy    import listener
from modelviewer import model_view  
from modelviewer.model_view import ModelViewer  

import pyinotify, functools
from pyinotify import WatchManager, Notifier, ProcessEvent
 
models = {}

def abort(a, b):
  print "[PDB_SERVER] Terminating PDB watcher"
  print "[PDB_SERVER] Terminating ModelViewer"
  model_view.terminate()
  import sys
  sys.exit(0)
  
class ProcessNew(ProcessEvent):
  def process_IN_CLOSE_WRITE(self, event):
    path = os.path.join(event.path, event.name)
    global models
    if ".obj" in event.name:
      name = event.name[:-len(".obj")]
      if name not in models.keys():
        print "[PDB_SERVER] Loading NEW file: %s" % name
        title = ""
        with open("../proteins/%s.name" % name) as f:
          title += f.read(80)
        m = ModelViewer(name, title)
        m.render()
        print "[PDB_SERVER] Loaded file."
        models[name] = m

def handle(n):
  print "handle(n)"

if __name__ == '__main__':
  signal.signal(signal.SIGINT,  abort) # ^C
  signal.signal(signal.SIGTERM, abort) # ^D

  # Initialise Omegalib
  print "[PDB_SERVER] Initialising Cyclops."
  model_view.init()

  print "[PDB_SERVER] Initialising PDB watcher."
  global models
  for f in os.listdir('../proteins'):
    if ".obj" in f:
      name = f[:-4]
      if name not in models.keys():
        print "[PDB_SERVER] Loading file: %s" % name
        title = ""
        with open("../proteins/%s.name" % name) as f:
          title += f.read(80)
        m = ModelViewer(name, title)
        m.render()
        print "[PDB_SERVER] Loaded file."
        models[name] = m
  """
  wm = WatchManager()
  notifier = Notifier(wm, ProcessNew()) 
  wm.add_watch('../proteins', pyinotify.IN_CLOSE_WRITE)
  handle_func = functools.partial(handle)   
  try:
    notifier.loop(daemonize=True, callback=handle_func, pid_file='/tmp/pyinotify.pid')
    pass
  except pyinotify.NotifierError, err:
    print >> sys.stderr, err
  """
