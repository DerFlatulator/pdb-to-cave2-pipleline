import sys, os

#import Midas
#Midas.export(filename=name, format='VRML')

pdb = sys.argv[1]

src = "./proteins/pdb/%s.pdb" % pdb
out = "./proteins/wrl/%s.wrl" % pdb

from chimera import runCommand as rc # use 'rc' as shorthand for runCommand

rc("open " + os.path.abspath(src))                    # open file
#rc("align ligand ~ligand")           # put ligand in front of remainder of molecule
#rc("focus ligand")                   # center/zoom ligand
#rc("surf")                           # surface receptor
#rc("preset apply interactive 1")     # make everything look nice
#rc("surftransp 15")                  # make the surface a little bit see-through
rc("export format VRML " + os.path.abspath(out))
#sleep(1)
rc("close all")
rc("stop now")

