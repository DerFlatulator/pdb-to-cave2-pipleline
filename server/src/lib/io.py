import sys, os

### http://code.activestate.com/recipes/577058/
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes":True,   "y":True,  "ye":True,
             "no":False,     "n":False}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "\
                             "(or 'y' or 'n').\n")
                             
def remove_all_files_in_dir(folder, relative=True):
  if (relative):
    folder = os.path.abspath(os.path.join(folder))
  for f in os.listdir(folder):
    file_path = os.path.join(folder, f)
    try:
      if os.path.isfile(file_path):
        os.unlink(file_path)
    except Exception, e:
      print e
     
### http://stackoverflow.com/a/566752/1280997     
def get_terminal_size():
  env = os.environ
  def ioctl_GWINSZ(fd):
    try:
      import fcntl, termios, struct
      cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
    except:
      return
    return cr
  cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
  if not cr:
    try:
      fd = os.open(os.ctermid(), os.O_RDONLY)
      cr = ioctl_GWINSZ(fd)
      os.close(fd)
    except:
      pass
  if not cr:
    cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

  return int(cr[1]), int(cr[0])
     
def clear_terminal_line(width=None):
  if width == None:
    (width, height) = get_terminal_size()
  sys.stdout.write(width * '\b')
  sys.stdout.write(width * ' ')
  sys.stdout.write('\r')      
  sys.stdout.flush()

def write(string):
  sys.stdout.write(string)
  sys.stdout.flush()  

def clear_terminal():
  os.system("clear")     
     
def get_lines_from_file(path):
  with open(path) as f:
    lines = f.read().splitlines()
  return lines
