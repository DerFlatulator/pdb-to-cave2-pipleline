import sys, signal
sys.dont_write_bytecode = True

from filecopy    import listener
  
def abort(a, b):
  listen.abort()
  sys.exit(0)

def abort_force(a, b):
  listen.abort_force()
  sys.exit(1)
  
if __name__ == '__main__':
  # Instanciate the listener
  listen = listener.Listener()
  
  # Handle termination signals
  print "[PDB_SERVER] Starting to listen for remote clients."
  signal.signal(signal.SIGINT,  abort)        # ^C
  print "[PDB_SERVER] Press Ctrl-C to close connections after file transfers complete; or"
  signal.signal(signal.SIGTERM, abort_force)  # ^D
  print "[PDB_SERVER] Press Crtl-D to to abruptly close connections and terminate"

  # Start listener
  for pdb, title in listen.listen():
    print "[PDB_SERVER] Received new protein [%s], will be loaded shortly..." % pdb

