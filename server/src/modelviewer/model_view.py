from math import *
from euclid import *
from omega import *
from cyclops import *
from omegaToolkit import *

#
# ==TODO==
# (= 1) PING!
###############################################################################

scene = getSceneManager()
sound = None
loadedModels = {}
menuButtons = {}
menu = None
lights = []

class ModelViewer:
  def __init__(self, pid, title):
    self.pid = pid
    self.mi = None
    self.title = title
    
  def render(self):
    # Set model info
    mi = ModelInfo()
    mi.name = self.pid
    mi.path = "../proteins/%s.obj" % self.pid 
    mi.size = -1
    mi.optimize = False
    self.mi = mi
    
    # Load model
    #scene.loadModelAsync(mi, "loaded('%s')" % self.pid)
    scene.loadModel(mi)
    # Set render info
    pid = self.pid
    title = self.title
    model = StaticObject.create(pid)
    loadedModels[pid] = model
    # model.setBoundingBoxVisible(True)
    model.setPosition(0, 2, -4)
    model.setEffect('colored -g 1.0 -s 20 -d blue')
    model.setVisible(True)

    # Add menu item
    menu_item = menu.addItem(MenuItemType.Checkbox)
    menu_item.setText('[%s] %s' % (pid, title))
    #menu_item.setCheckable(True)
    menu_item.setChecked(True)
    menu.show()
    Handler.rig(menu_item, pid)
    
    global menuButtons
    menuButtons[pid] = menu_item
    if (sound): #untested
      instance = SoundInstance(sound)
      instance.play()
    else:
      print "New model loaded!"  


###############################################################################

class Handler:
  @staticmethod
  def onEvent():
    pass
    #e = getEvent()
    #e_type = e.getType()

  @staticmethod
  def rig(menu_item, pid):
    menu_item.setCommand("model_view.Handler.toggleProtein('%s');" % pid)
    
  @staticmethod
  def toggleProtein(pid):
    state = menuButtons[pid].isChecked()
    loadedModels[pid].setVisible(state)
    loadedModels[pid].setSelected(state)
    menuButtons[pid].setChecked(state)   
	#interactor.setSceneNode(curModel)
	


setEventFunction(Handler.onEvent)

def init():
  init_lights()
  init_menu()
  init_skybox()
  init_wand()
  init_sound()

  # < test
  #mv = ModelViewer("4Q21", "DEFAULT")
  #mv.render()
  #   test > 

def terminate():
  #SoundEnvironment.stopSoundServer()
  oclean()
  oexit()
  import sys
  sys.exit(0)

def init_lights():
  global lights
  lights = [Light.create(), Light.create()]

  lights[0].setColor(Color("#807070"))
  lights[0].setAmbient(Color("#202020"))
  lights[0].setPosition(Vector3(0, 20, 5))

  lights[1].setColor(Color("#707090"))
  lights[1].setPosition(Vector3(0, -20, -5))
  
  for light in lights:
    light.setEnabled(True)
 
  #scene.setMainLight(lights[0])

def init_menu():
  # setup menu
  menu_manager = MenuManager.createAndInitialize()
  global menu
  menu = menu_manager.createMenu("main")
  menu_manager.setMainMenu(menu)

  # setup the skybox menu
  miSkyboxes = menu.addItem(MenuItemType.SubMenu)
  miSkyboxes.setText("Background");
  menuSkyboxes = miSkyboxes.getSubMenu()
  misb = menuSkyboxes.addItem(MenuItemType.Button)
  misb.setText("Gradient 1")
  misb.setCommand('skybox.loadCubeMap("cubemaps/gradient1", "png")')
  misb = menuSkyboxes.addItem(MenuItemType.Button)
  misb.setText("Gradient 2")
  misb.setCommand('skybox.loadCubeMap("cubemaps/gradient2", "png")')
  misb = menuSkyboxes.addItem(MenuItemType.Button)
  misb.setText("Grid 1")
  misb.setCommand('skybox.loadCubeMap("cubemaps/grid3", "png")')
  misb = menuSkyboxes.addItem(MenuItemType.Button)
  misb.setText("Grid 2")
  misb.setCommand('skybox.loadCubeMap("cubemaps/grid4", "png")')
  misb = menuSkyboxes.addItem(MenuItemType.Button)

  miPDBs = menu.addItem(MenuItemType.SubMenu)
  miPDBs.setText("Proteins")
  menu = miPDBs.getSubMenu()

def init_skybox():
  # setup skybox
  global skybox
  skybox = Skybox()
  skybox.loadCubeMap("cubemaps/grid4", "png")
  scene.setSkyBox(skybox)

def init_wand():
  # create the default interactor
  interactor = ToolkitUtils.setupInteractor("config/interactor")
  # enable wand
  scene.displayWand(0, 1)

def init_sound():
  if isSoundEnabled():
    SoundEnvironment.startSoundServer()
    global sound
    sound = getSoundEnvironment().loadSoundFromFile("ping", "../sounds/transmission.wav")
    
